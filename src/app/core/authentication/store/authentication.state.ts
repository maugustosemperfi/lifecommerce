import { State, Action, StateContext, Store } from '@ngxs/store';
import {
  AuthLogin,
  AuthLogout,
  AuthLoginSuccess,
  AuthLoginFail
} from '@app/core/authentication/store/authentication.actions';
import { AuthenticationModel } from '@app/core/authentication/authentication.model';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Navigate } from '@ngxs/router-plugin';


export interface AuthenticationStateModel {
  user: AuthenticationModel | null;
  token: string;
  loggedIn: boolean;
}

@State<AuthenticationStateModel>({
  name: 'authenticationState',
  defaults: {
    user: null,
    token: null,
    loggedIn: false
  }
})
export class AuthenticationState {

  constructor(private authService: AuthenticationService, private store: Store) {}

  @Action(AuthLogin)
  authLogin(ctx: StateContext<AuthenticationStateModel>, payload: AuthLogin) {
    ctx.patchState({
      user: payload.auth
    });

    this.authService.login(payload.auth);
  }

  @Action(AuthLogout)
  authLogout(ctx: StateContext<AuthenticationStateModel>) {
    ctx.patchState({
      user: null,
      token: null
    });
  }

  @Action(AuthLoginSuccess)
  authLoginSuccess(ctx: StateContext<AuthenticationStateModel>, payload: AuthLoginSuccess) {
    ctx.patchState({
      user: payload.auth,
      token: payload.token
    });

    this.store.dispatch(new Navigate(['/home']));
  }

  @Action(AuthLoginFail)
  authLoginFail(ctx: StateContext<AuthenticationStateModel>) {
    ctx.patchState({
      token: null
    });
  }
}
