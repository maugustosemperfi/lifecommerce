import { AuthenticationModel } from '@app/core/authentication/authentication.model';

export class AuthLogin {
  static readonly type = '[AUTHENTICATION] LOGIN';
  constructor(public readonly auth: AuthenticationModel) {}
}

export class AuthLogout {
  static readonly type = '[AUTHENTICATION] LOGOUT';
  constructor() {}
}

export class AuthLoginSuccess {
  static readonly type = '[AUTHENTICATION] LOGIN SUCCESS';
  constructor(public readonly auth: AuthenticationModel, public readonly token: string) {}
}

export class AuthLoginFail {
  static readonly type = '[AUTHENTICATION] LOGIN FAIL';
}
