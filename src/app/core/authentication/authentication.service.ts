import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AuthenticationStateModel } from '@app/core/authentication/store/authentication.state';
import { environment } from '@env/environment';
import { Store } from '@ngxs/store';
import { AuthLoginSuccess, AuthLoginFail } from '@app/core/authentication/store/authentication.actions';
import { AuthenticationModel } from '@app/core/authentication/authentication.model';
import { HttpClient } from '@angular/common/http';

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

export interface LoginResponse {
  auth: AuthenticationModel;
  token: string;
}

const credentialsKey = 'credentials';

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable()
export class AuthenticationService {

  private _credentials: Credentials | null;

  constructor(private http: HttpClient, private ngxsStore: Store) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  login(auth: AuthenticationModel) {
    return this.http.post(`user/login`, auth).toPromise()
      .then(data => data as LoginResponse)
      .then(data => {
        this.ngxsStore.dispatch(new AuthLoginSuccess(data.auth, data.token));
      })
      .catch(error => {
        this.ngxsStore.dispatch(new AuthLoginFail());
      });
  }

  /**
   * Logs out the user and clear credentials.
   * @return {Observable<boolean>} True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.setCredentials();
    return of(true);
  }

  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  /**
   * Gets the user credentials.
   * @return {Credentials} The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials | null {
    return this._credentials;
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param {Credentials=} credentials The user credentials.
   * @param {boolean=} remember True to remember credentials across sessions.
   */
  private setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem(credentialsKey);
    }
  }

}
