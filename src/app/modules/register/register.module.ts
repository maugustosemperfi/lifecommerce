import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { SharedModule } from '@app/shared';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { RegisterServiceService } from '@app/modules/register/register-service.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TranslateModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    RegisterRoutingModule
  ],
  declarations: [RegisterComponent],
  providers: [
    RegisterServiceService
  ]
})
export class RegisterModule { }
