import { Injectable } from '@angular/core';
import { RegisterModule } from '@app/modules/register/register.module';
import { HttpClient } from '@angular/common/http';
import { User } from '@app/shared/models/user.model';

@Injectable({
  providedIn: RegisterModule
})
export class RegisterServiceService {

  constructor(private http: HttpClient) { }

  register(user: User) {
    return this.http.post('user/create', user).toPromise()
      .then(data => {
        console.log(data);
      })
      .catch(error => {
        console.log(error);
      });
  }
}
