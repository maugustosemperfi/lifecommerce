import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterServiceService } from '@app/modules/register/register-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private fb: FormBuilder, private registerService: RegisterServiceService) { }

  ngOnInit() {
    this.generateForm();
  }

  generateForm() {
    this.registerForm = this.fb.group({
      country: ['', Validators.required],
      voucherCode: [''],
      name: ['', Validators.required],
      birthdate: [{value: '', disabled: true}, [Validators.required]],
      ssn: [''],
      phone: ['', Validators.required],
      email: ['', [Validators.required]],
      emailConfirmation: ['', [Validators.required]],
      user: ['', Validators.required],
      password: ['', [Validators.required]],
      passwordConfirmation: ['', [Validators.required]]
    });
  }

  register() {
    console.log('?');
    this.registerService.register(this.registerForm.value);
  }

}
