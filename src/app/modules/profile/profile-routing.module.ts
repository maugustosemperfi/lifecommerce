import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from '@app/modules/profile/profile.component';
import { TermsComponent } from '@app/modules/profile/terms/terms.component';
import { DocumentsComponent } from '@app/modules/profile/documents/documents.component';
import { UsersComponent } from '@app/modules/profile/users/users.component';

const routes: Routes = [
  { path: 'profile', component: ProfileComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'documents', component: DocumentsComponent },
  { path: 'users', component: UsersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
