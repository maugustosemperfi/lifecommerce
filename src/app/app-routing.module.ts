import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Route } from '@app/core';

const routes: Routes = [
  Route.withShell([
    { path: 'about', loadChildren: 'app/about/about.module#AboutModule' },
    { path: 'register', loadChildren: './modules/register/register.module#RegisterModule' },
    { path: 'profile', loadChildren: './modules/profile/profile.module#ProfileModule' },
    { path: 'materials', loadChildren: './modules/materials/materials.module#MaterialsModule' },
    { path: 'store', loadChildren: './modules/store/store.module#StoreModule' },
    { path: 'contact', loadChildren: './modules/contact/contact.module#ContactModule'},
    { path: 'ticket', loadChildren: './modules/ticket/ticket.module#TicketModule' }
  ]),
  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
